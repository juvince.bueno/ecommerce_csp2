// [SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/products');
	const auth = require('../auth')
	const {verify,verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] POST
	route.post('/create', verify, (req, res)=>{
        let productDetails = req.body;
        controller.createProduct(productDetails).then(outcome =>{
            res.send(outcome);
        });
    });


// [SECTION] [GET] Routes
	// active products
	route.get('/all', (req, res) => {
		controller.getAllActiveProducts().then(outcome => {
			res.send(outcome);
		});
	});

	// single product
	route.get('/:productid', (req, res) => {	
		let productID = req.params.productid;
		controller.getProduct(productID).then(result => {			
			res.send(result);
		});
	});

// [SECTION] [PUT] Routes
	route.put('/:productid', verify, (req, res) => {
		
      let productid = req.params.productid; 
      let details = req.body;
      
      let name = details.productName; 
      let desc = details.description;
      let price = details.productPrice;       
      if (name  !== '' && desc !== '' && price !== '') {
        controller.updateProduct(productid, details).then(outcome => {
            res.send(outcome); 
        });      
      } else {
        res.send('Incorrect Input, Make sure details are complete');
      }

	});

// [Deactivate]
 	route.put('/:productid/archive', verify, (req, res) => {
        
      let productId = req.params.productid; 

      controller.deactivateProduct(productId).then(resultOfTheFunction => {

       	  res.send(resultOfTheFunction);
      });
  });

  // [Reactivate]
  		route.put('/:productid/reactivate', verify, (req, res) => {
    	    
      	let productId = req.params.productid; 

      	controller.reactivateProduct(productId).then(resultOfTheFunction => {
          
         res.send(resultOfTheFunction);
      });
  });

// [SECTION] Export Route System
	module.exports = route;