// [SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/orders');
	const auth = require('../auth')
	const {verify,verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = exp.Router();

// [POST]
	route.post('/create', verify, (req, res) => {
		let orderDetails = req.body;
		let user = req.user;
		controller.createOrder(user, orderDetails).then(outcome => {
			res.send(outcome);
			console.log(outcome);
		});
	});

// [SECTION] Export Route System
	module.exports = route;