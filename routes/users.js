// [SECTION]Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/users');
	const auth = require("../auth")
	const {verify,verifyAdmin} = auth;

// [SECTION]Routing Component
	const route = exp.Router();

// [SECTION] Post Route
	// user registration
	route.post('/register', (req, res) => {
		let userDetails = req.body;
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome);
		});
	});

	// login
	route.post('/login', controller.loginUser);

// [SECTION] GET Route
	route.get('/all', (req,res) => {
		controller.getAllUsers().then(outcome => {
			res.send(outcome);
		});
	});


// [SECTION]Expose Route System
	module.exports = route;