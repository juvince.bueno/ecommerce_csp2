// [SECTION] Dependencies and Modules
	const Product = require('../models/Product');
	const auth = require('../auth');
	const bcrypt = require("bcrypt");
  const dotenv = require("dotenv"); 


// [SECTION] Create Products
	module.exports.createProduct = (info) => {
		
			let name = info.productName;
			let desc = info.description;
			let price = info.productPrice;

			let newProduct = new Product({
				productName: name,
				description: desc,
				productPrice: price
			})

			return newProduct.save().then((savedProduct, error) => {
				if(error){
					return 'Failed to save the product';
				} else {
					return savedProduct;
				}
			});

		

	}

	// [Deactivate]
   module.exports.deactivateProduct = (productid) => {
     let updates = {
       isActive: false
     } 
     
     return Product.findByIdAndUpdate(productid, updates).then((archived, error) => { 
        if (archived) {
          return `The Product ${productid} has been deactivated`;
        } else {
           return 'Failed to archive course'; 

        };
     });
   };

  // [Reactivate]

  module.exports.reactivateProduct = (productid) => {
     let updates = {
       isActive: true
     } 
     
     return Product.findByIdAndUpdate(productid, updates).then((archived, error) => { 
        if (archived) {
          return `The Product ${productid} has been reactivated`;
        } else {
           return 'Failed to reactivate course'; 

        };
     });
   };

// [SECTION] Retrieve Function
	// retrive all active products
	module.exports.getAllActiveProducts = () =>{
		return Product.find({isActive:true}).then(results => {
			return results;
		});
	};

	// retrieve single product
	module.exports.getProduct = (productid) => {		
		return Product.findById(productid).then(resultofQuery => {
			return resultofQuery;
		});
	};

// [SECTION] Update Function
	module.exports.updateProduct = (productid, details) => {
		
      let name = details.productName; 
      let desc = details.description;
      let price = details.productPrice;   
       
      let productUpdate = {
        productName: name,
        description: desc,
        productPrice: price
      }
      
      return Product.findByIdAndUpdate(productid, productUpdate).then((productUpdated, err) => {
          
          if (err) {
            return 'Failed to update Product'; 
          } else {
            return 'Successfully Updated Product'; 
          }
      });

	}