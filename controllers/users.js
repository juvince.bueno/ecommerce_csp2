//[SECTION] Dependencies and Modules
  // const User = require('../models/User');
  const auth = require('../auth')
  const User = require('../models/User');
  // const Order = require('..models/Order');
  const bcrypt = require("bcrypt");
  const dotenv = require("dotenv"); 

//[SECTION] Environment Setup
  dotenv.config();
  const salt = 10;

//[SECTION] Functionalities[Create]
  // add user
  module.exports.registerUser = (data) =>{
    let fname = data.firstname;
    let lname = data.lastname;
    let email = data.email;
    let pass = data.password;
    let newUser = new User({
      firstname: fname,
      lastname: lname,
      email: email,
      password: bcrypt.hashSync(pass, salt)
    });
    return newUser.save().then((user, rejected) => {
      if(user){
        return user;
      } else{
        return 'Failed to Register a new account';
      };
    });
  }

  // login user
  module.exports.loginUser = (req, res) => {

    User.findOne({email: req.body.email}).then(founduser => {
      if (founduser === null){
        return res.send('User not found');

      } else{

        // console.log(founduser);
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, founduser.password);
       
        if(isPasswordCorrect){
          return res.send({accessToken: auth.createAccessToken(founduser)});
        } else{
          return res.send("Incorrect Password");
        }
      }
    }).catch(err => res.send(err));
  };

// [SECTION]Functionalities[Update]

// [SECTION]Retrieve
  module.exports.getAllUsers = () => {
    return User.find().then(results => {
      return results;
    });
  }