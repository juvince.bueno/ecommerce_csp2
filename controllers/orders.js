// [SECTION] Dependencies and Modules
	const express = require("express");
	const Product = require('../models/Product');
	const Order = require("./../models/Order");
	const User = require("./../models/User");

// [SECTION]Functionalities Create
	module.exports.createOrder = async(req, res) => {

		let userStatus = req.isAdmin;
		let userid = req.id;
		let productid = res.id;
		let prodquantity = res.quantity; 

		if(userStatus === true){
			return res.send("Action Forbidden");

		}

		let productFinder = await Product.findById(productid).then((foundProduct) => {
			return foundProduct;

		})
		.catch((err) => err.message);
		console.log(productFinder);

		let newOrder = new Order({
			totalAmount: (productFinder.productPrice * prodquantity),
			orderBy: userid,
			orders: [
				{
					productId: productFinder._id,
					quantity: prodquantity
				},
			],
		});
		// console.log(newOrder());

	let orderCreatedResult = await newOrder
	    .save()
	    .then((orderCreated) => {
	      return orderCreated;
	    });
	  
	  if (orderCreatedResult !== null) {
	    
		    let updateUser = {
		      orders: [
		        {
		          orderID: orderCreatedResult._id,
		        },
		      ],
		    };
		return User.findByIdAndUpdate(userId,{$push : updateUser})
      	.then((foundUser) => {
        
        return (foundUser)
      	})
      	.catch((err) => err.message);
      }
  }