// [SECTION]Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION]Schema
const orderSchema = new mongoose.Schema({
    orderBy:{
        type: String,
        required: [true, "User ID is required"]
    },
    orders:[
        {
            productId:{
                type: String,
                required: [true, "Product ID is required"]
            },
            quantity: {
                type: Number,
                required : [true, "Order quantity is required"]
            }
        }
    ],
    totalAmount: {
        type: Number,
        required : [true, "Total amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    isComplete:  {
        type: Boolean,
        default: false
    }
});

// [SECTION]Model
module.exports = mongoose.model("Order", orderSchema);