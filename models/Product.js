/* Product
 - Name (string)
 - Description (string)
 - Price (number)
 - isActive (Boolean - defaults to true)
 - createdOn (Date - defaults to current date of creation)*/

 // [SECTION] Dependencies and Modules
 	const mongoose = require('mongoose');

//[SECTION] Blueprint Schema
	const productSchema = new mongoose.Schema({
		productName:{
			type: String,
			require: [true, 'product name is required']
		},
		description:{
			type: String,
			require: [true, 'Please Provide Product Description']
		},
		productPrice:{
			type: Number,
			default: 0.00
		},
		isActive:{
			type: Boolean,
			default: true,
		},
		createdOn:{
			type: Date,
			default: new Date()
		}

	});


//[SECTION]Model
  const Product = mongoose.model("Product", productSchema);
  module.exports = Product;