// [SECTION]Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION]Schema
	const userSchema = new mongoose.Schema({
		firstname: {
			type: String,
			required: [true, 'Please provide your firstname']
		},
		lastname: {
			type: String,
			required: [true, 'Please provide your lastname']
		},
		email: {
			type: String,
			required: [true, 'Email Address is required']
		},
		password: {
			type: String,
			required: [true, 'Enter your password']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},

		purchases: [
			{
				orderid: {
					type: String,
					required: [true]
				},

				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});
// [SECTION]Model
	const User = mongoose.model("User", userSchema);
	module.exports = User;